% !TEX root = ../main.tex

\section{Results}
\label{sec:results}

This section describes our experimental results: the design and implementation of a DevOps pipeline for an opensource java web application.
\subsection{Reverse engineering of the chosen web application}
In the following subsection, we will briefly describe the product and some of the reverse engineering results.\\
For this project we had to respect some constraint for the product itself, it should be an open-source java-based web application. A good candidate has been the Zipkin application\cite{zipkin2019}. This application is a web-based Java application that collects and allows lookup of distributed system tracing information (e.g. timings, API calls, application logs).\\
The application itself is composed of multiple services that are served and consumed by the Zipkin API stack. The targeted product will be the Zipkin API stack, also referenced as Zipkin server.

\subsection{Pipeline functional and Non-functional requirements}
Once the product has been chosen, we have defined the functional and non-functional requirements of our pipeline.\\
As per minimum DevOps principles and based on the project constraint\cite{devops2019}, our selected functional requirements are the following:
\begin{enumerate}[label=\textbf{FR\arabic*}, leftmargin=*]
\item Concurrent work on the product source code
\item Integration of different teams work
\item Code is the same during different stages
\item Different environments serve a particular purpose
\item Environment are isolated from each other
\item A deployment candidate is produced when the pipeline is successful
\item A deployment can be rolled back easily
\item Pipeline provides feedback on its execution
\end{enumerate}
For the non-functional requirements, we focused on the time efficiency and adaptability of the pipeline as we think that those characteristics are important aspects of DevOps implementations.\\
Thus the selected non-functional requirements for the pipeline are:
\begin{enumerate}[label=\textbf{NFR\arabic*}, leftmargin=*]
\item \textbf{Performance efficiency} with time behaviour, that is, the commit stage should take less than 10 minutes and the overall pipeline should be run under 60 minutes.
\item \textbf{Reliability} with recoverability that is, the fact that the pipeline and its environment can be automatically recreated from backed up scripts in less than four hours.
\item  \textbf{Maintainability} with Reusability that is, the fact that the pipeline and its environment can be deployed to be used with another product in less than four hours.
\end{enumerate}

\subsection{Formal definition and design of the pipeline}
After the definition of functional and non functional requirement we can formally define and design our pipeline.\\
Our artefact set is defined as follow:
$ART = \{source\_code, configuration, java\_class, jar, $\\
$docker\_image, unit\_test\_result, auto\_uat\_result, $\\
$manual\_uat\_result\}$.\\
The artefact available in the source repository are $Art_0 = \{source\_code, configuration\}$
The state set \textit{STA} is defined as follow:
$\{00,01,10,00\}$ which correspond respectively to:
\textit{staging not deploy and production\ not deploy, staging deploy and production not deploy, staging not deploy and production deploy, staging deploy and production deploy}.\\
The success flag set is: $SFL=\{TRUE,FALSE\}$.\\
Then we have identified 8 activities distributed among 3 different stages:\\
The \textbf{Commit stage} has the following 4 activities:
\begin{itemize}
\item \underline{\textit{build}}: $ STA \times source\_code \rightarrow SFL \times STA \times java\_class $
\item \underline{\textit{unit\_test}}: $ STA \times java\_class \rightarrow SFL \times STA \times unit\_test\_result $
\item \underline{\textit{jar\_package}}: $ STA \times java\_class \rightarrow SFL \times STA \times jar $
\item \underline{\textit{docker\_package}}: $ STA \times jar \rightarrow SFL \times STA \times docker\_image $
\end{itemize}
The \textbf{testing stage} has the following 3 activities:
\begin{itemize}
\item \underline{\textit{staging\_deploy}}: $ STA \times docker\_image \rightarrow SFL \times STA$
\item \underline{\textit{auto\_acceptance\_test}}: $ STA \times source\_code \rightarrow SFL \times STA \times auto\_uat\_result$
\item \underline{\textit{manual\_acceptance\_test}}: $ STA \rightarrow SFL \times STA \times manual\_uat\_result$
\end{itemize}
Finally the \textbf{release stage} has the following activity:
\begin{itemize}
    \item \underline{\textit{production\_deploy}}: $STA \times docker\_image \rightarrow SFL \times STA$\\
\end{itemize}

We define constraints of the $production\_deploy$ and $staging\_deploy$ activities as followed:
\begin{itemize}
    \item $deploy\_staging \subseteq ((STA \times docker\_image) \times ( \{TRUE\} \times \{10,11\}) \cup ((STA \times docker\_image) \times (\{FALSE\} \times \{01,00\}))$
    \item $deploy\_production \subseteq ((STA \times docker\_image) \times ( \{TRUE\} \times \{01,11\}) \cup ((STA \times docker\_image) \times (\{FALSE\} \times \{00,10\}))$
\end{itemize}
This constrains express that after attempting to deploy on an environment, either the application is indeed deployed or the activity is failed (i.e. success flag to false).

The partial order of the activities is defined by:
\begin{itemize}
    \item $ build < unit\_test < jar\_package < docker\_package < staging\_deploy$
    \item $ staging\_deploy < auto\_acceptance\_test < production\_deploy$
    \item $ staging\_deploy < manual\_acceptance\_test < production\_deploy $
\end{itemize}
All activities are automatically executed apart from the $manual\_acceptance\_test$.

A graphical representation of the partial ordering of activities is given in Figure \ref{fig:pipeline_diagram}.

\begin{figure}[h]
\begin{center}
  \includegraphics[width=0.5\textwidth]{fig/pipeline_diagram}
  \caption{BPMN 2.0 diagram of the pipeline process}
  \label{fig:pipeline_diagram}
\end{center}
\end{figure}

\subsection{Pipeline environment design}
The next step in our DevOps pipeline creation is the design of the different environments that will support the activities of our pipeline.

\begin{figure*}[ht]
\centering
  \includegraphics[width=\textwidth]{fig/environment_diagram}
  \caption{UML deployment diagram of the DevOps environment}
  \label{fig:environment_diagram}
\end{figure*}


\subsubsection{Environment isolation}
As per functional requirements (FR5), our different environments will be isolated from each other in the sense that a particular environment state will have no incidence on another one. However, they will still be able to communicate.\\
We have identified 4 different environments that support all the activities of the pipeline (FR4) as shown in Figure \ref{fig:environment_diagram}. Please note that in our approach an environment is a (virtual) machine. Each of the 3 first virtual machine (i.e. environment) runs on the same physical machine.
\begin{itemize}
\item The \textbf{integration environment} will support the commit stage activities (\textit{build, unit\_test, jar\_package, docker\_package}) as well as some tool that support the \textit{manual\_acceptance\_test} activity.
It also holds the CI components that orchestrate the activities and satisfy FR3, FR6 and FR7. The version control system fulfils the requirements FR1, FR2.
The ticket management system helps to organise the team and to fulfil FR1. The containerisation engine is used to execute the different activities of the pipeline.
\item The \textbf{production environment} will support the release stage activity (\textit{production\_deploy}). To allow the deployment of the artefact this environment will hold the necessary tools to support container deployment.
\item The \textbf{staging environment} will support the \textit{staging\_deploy} activity and will take part in the \textit{acceptance test} activity. As per good DevOps practice \cite{HumbleFarley10}(p. 258) the staging environment will be as close as the production environment, that is that it will hold the same tools and hardware specification as the production environment.
\item Finally the \textbf{development environment} is a bit particular because it holds the necessary tools to locally develop the application that is, to modify the source code, to run a development version locally, and to execute version control actions. This environment runs in the development team personal machine.
\end{itemize}
The usage of containerisation technology allows us to use different technology stacks without installing any additional software on the environments, hence improving the reusability of the environment and the pipeline (NFR3).

\subsection{Implementation of environment components}
Once the overall environments are designed and the components are specified we can choose the implementation of the tools.\\
For the container engine, provider and registry, we have chosen Docker\footnote{https://www.docker.com/} as it is widely used for lightweight container deployment and specifically by the DevOps community.\\
Then we have decided to use the GitLab community edition\footnote{https://about.gitlab.com/} which high number of components allows to cover version control system, ticketing system, pipeline execution environment (i.e. continuous integration server), and artefact repository.\\
Finally, for the operating system of each environment we decided to used Debian\footnote{https://www.debian.org/index.fr.html}, an open-source Linux distribution well known for its stability.

To improve their recoverability (NFR2), we have automated the creation of environments using provisioning and virtualisation. As a result, only two manual steps are necessary (re)create a particular environment. The process and sources to recreate the environment can be found in \cite{environment_sources}. 

\subsection{Pipeline scripting}
\subsubsection{Activity scripting}
The final part of our pipeline implementation is the actual scripting of each activity and aggregate them in the overall pipeline script.\\
The first activities to script are the one of the \textbf{commit stage}. \\
As previously specified, the commit stage holds the \textit{build, unit\_test, jar\_package} and \textit{docker\_package} activities. The \textit{build}, \textit{ unit\_test,} and \textit{jar\_package} are executed through the maven\footnote{https://maven.apache.org/guides/introduction/introduction-to-the-lifecycle.html} tool respectively \texttt{mvn compile} , \texttt{mvn test}, \texttt{mvn package}.

The build of the container image is done using docker: \texttt{docker build} and finally, \texttt{docker push} to push the docker image (i.e. the candidate artefact) to our artefact repository.

The next stage is the \textbf{testing stage}.
This one holds the \textit{staging\_deploy}, \textit{auto\_acceptance\_test} and \textit{manual\_acceptance\_test} activities. The first one is done through docker-compose \texttt{docker-compose pull} and \texttt{docker-compose up}.\\
At the current development stage we have implemented only one automatic acceptance test (smoke test) that is run through a bash script. The rest of the acceptance tests are done manually.

The final stage is the \textbf{release stage} with the \textit{production\_deploy} activity that is scripted as the previous \textit{staging\_deploy} through docker-compose.

\subsubsection{Pipeline script}
To orchestrate the execution of each of the previously defined activities we used the built-in pipeline scripting specification of GitLab\cite{gitlabpipeline}. This allows us to define the activities orders and the trigger method of each stage (i.e. manual or automatic).\\
Other technical aspects like cache and environment variable are defined through this script but we will not go into these details.
The pipeline script can be found in \cite{pipeline_sources}.


\subsection{Measurements}

This section presents the empirical study and its results. All environment and operation are run on a 2018 Mac mini (3.2 GHz Intel Core i7, 32 GB DDR4).

The first scenario aims to measure the time performance efficiency. For that, we first create the environment following the steps described in \cite{environment_process}. Then we import the project and the pipeline following this description. Finally, we commit a modification using the web ide provided by GitLab. For the measurement, we relay on GitLab/CI time measurement. We repeat this last operation 3 times. The execution times of the commit stage are 1m18s, 1m11s and 1m13s which is less than our 10 minutes threshold. The execution times of the entire pipeline are 2m52s, 2m40s and 3m23s which is less than our one-hour threshold. Therefore the performance efficiency requirement (NFR1) is satisfied.

The second scenario aims to measure the recoverability. In this scenario, we suppose that all the environments have been destroyed and we only have access to a backup of the source repository. We measure the time needed to recreate the environments, import the source of the product and redeploy the product as fast as possible. We suppose that the person performing these tasks is a DevOps engineer. The environments are recreated using process \cite{environment_process}, the project is imported using process \cite{project_process}. The process of redeploying the app is automatic at the end of the project import. We managed to recreate the environment in 30 minutes and to import the project in 10 minutes for a total of 40 minutes. This is significantly less than the 4-hour threshold fixed. Hence the recoverability requirement (NFR2) is satisfied.

The third scenario aims to measure the reusability. We measure the time necessary to adapt the pipeline to a similar product named PetClinic\cite{petclinic2019} used by our colleagues on a similar project. The starting point is that the environment is already deployed, the pipeline is running for Zipkin product and we have access to the source code of PetClinic. To accelerate the process we did not follow the whole method described in this paper. Instead, the difference between Zipkin and PetClinic have been identified and the pipeline incrementally adapted. The time was measured between the first look at PetClinic source code and its deployment on the production server. This adaptation took us 22 minutes. This is significantly less than the 4-hour threshold. Therefore the reusability requirement (NFR3) is satisfied.
