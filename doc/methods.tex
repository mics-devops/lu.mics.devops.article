% !TEX root = ../main.tex

\section{Methods}
\label{sec:method}

\subsection{Reverse engineering of the chosen web application}

It is necessary to gather information on the requirements and design of the web application.
Access to the requirements and design documents is useful but not necessary.
It is also possible to extract this information from the code, the configuration files and the operational system.

First of all, it is important to determine the set of components the application depends on (such as databases, authentication server). This set of components together with the web application is later called the system to be deployed.

Later, the functional and non-functional requirements of the web application have to be gathered, with a particular emphasis on the one that concerns the operation.
In terms of functional requirements, it is only necessary to create the smallest set of requirements such that all the components are used if the function is executed.
It is harder, yet not impossible to determine the non-functional requirements from the code or the operational system.

\subsection{Pipeline Functional requirement definition}
The first step in the requirement definition is the functional definition of the pipeline. \\
To follow DevOps principles of \cite{HumbleFarley10} (p. 105) a minimum set of functional requirements should be ensured.\\
The first one is the ability to produce a valid candidate for further deployment. The second one is the ability to provide feedback during each stage of the pipeline. Finally, the pipeline should be able to allow deploy or rollback a certain candidate.\\
Other functional requirements might be specified and are specific to the client needs (e.g. allow concurrent work, automatic sequencing of stages).


\subsection{Pipeline Non-functional requirement definition}

The ISO standard \cite{isoiec2010} proposes a complete set of non-functional requirements.
The non-functional requirements targeted by the pipeline are chosen from this set.
These requirements are stated in natural language together with their metric and their metric unit.
The satisfying condition of each requirement is defined based on the selected metric.

\subsection{Definition of a formal framework for pipeline design}

This section provides all concepts necessary to formally describe a pipeline. A pipeline is a set of activities taking artefacts of certain types as input and producing a set of artefacts of certain types as output. An activity can be successful or not. An activity can also alter the environment state.

The three first set proposed are the sets of \textit{artefact types}, of \textit{states} and of possible values for the \textit{success flag}.\\

\begin{definition}
  The basic sets of the \frameworkname are defined as a disjoint subset of a given universe $U$:
  \begin{enumerate}
    \item $ART \in \powerset(U)$ the set of all artefact types.
    \item Let $art \in ART$, $art$ is the set of all possible artefacts of the artefact type $art$.
    \item $STA \in \powerset(U)$ the set of all possible states of the environments.
    \item $SFL \in \powerset(U) = \{TRUE,FALSE\}$ the set of possible value for the success flag that represents the successful execution or not of the activity.
  \end{enumerate}
\end{definition}


\begin{remark}
  \begin{enumerate}
    \item $art, art_0, art_1... art_n$  will denote an element of $ART$.
    \item $sta, sta_0, sta_1... sta_n$  will denote an element of $STA$.
  \end{enumerate}
\end{remark}

\begin{definition}
  The set of artefact types available in the repositories at the beginning of the pipeline is:
  $Art_0 \subseteq ART$
\end{definition}

\begin{definition}
  Let $ART = \{art_0... art_i\}$, $STA = \{sta_0...sta_m\}$ and $SFL = \{TRUE,FALSE\} $
  \begin{enumerate}
    \item An activity denoted $act_i$ is a relation such that $act_i : STA \bigtimes art_{d0} \bigtimes ... \bigtimes art_{dn} \rightarrow SFL \bigtimes STA \bigtimes art_{c0} \bigtimes ... \bigtimes art_{cm}$.\\
    $art_{d0} \bigtimes ... \bigtimes art_{dn}$ and $art_{c0} \bigtimes ... \bigtimes art_{cm}$ represents respectively the input and output artefact types of the activity.
    \item $ACT \in \powerset(U)$ is the set of all defined activities $act_i$.
  \end{enumerate}


\end{definition}

\begin{remark}
  \begin{enumerate}
    \item $act, act_0, act_1... act_n$  will denote an element of $ACT$.
  \end{enumerate}
\end{remark}

\begin{example}
  Let us build the model of a simple pipeline with one activity that takes as input the source code and a dependency jar and returns as output a jar.\\
  \begin{enumerate}
    \item $ART = \{ source, djars, jars\}$ defines the type of artefacts used in the pipeline.
    \item $STA = \{ sta_0, sta_1 \}$ defines the states in the pipeline.\\
    \item $ACT = \{ build\}$ with $build : STA \bigtimes source \bigtimes djars \rightarrow SFL \bigtimes STA \bigtimes jars $
  \end{enumerate}

\end{example}

\begin{remark}
  When engineering a pipeline, the goal is not to give a complete definition of the activities relation $act$ but only to define its domain and codomain.
  However, a partial definition can be given to express constraints.
\end{remark}

\begin{example}
  The constraint that ``the $build$ activity can only be a success if the input state is $sta_0$'' can be express as follow.
  $ build \subseteq \Big( ( \{sta_i \in STA | sta_i \neq sta_0\} \bigtimes source \bigtimes djars) \bigtimes (\{FALSE\} \bigtimes STA \bigtimes jars) \cup ( \{sta_0\} \bigtimes source \bigtimes djars) \bigtimes (FLG \bigtimes STA \bigtimes jars)\Big)$
\end{example}




\begin{definition}
     A partial order relation $\leq$ is defined over the set of activities $ACT$.\\
    $act_0, act_1 \in ACT, act_0 \leq act_1$ means that activity $act_0$ must be executed before $act_1$.\\
    For every activity $act_x$, each of its input artefact types is in the set of the artefact types in $Art_0$, or is the result of a previous activity: \\
    $\forall (act_x : STA \bigtimes art_{xd0} \bigtimes ... \bigtimes art_{xdn} \rightarrow SFL \bigtimes STA \bigtimes art_{xc0} \bigtimes ... \bigtimes art_{xcm} ) \in  ACT | \underset{i \in \{0..n\}}{\forall} art_{xdi} | (art_{xdi} \in Art_0) \bigvee (\exists (act_y : STA \bigtimes art_{yd0} \bigtimes ... \bigtimes art_{ydn'} \rightarrow SFL \bigtimes STA \bigtimes art_{yc0} \bigtimes art_{ycm'}) \in  ACT | ( act_y < act_x \bigwedge ( \underset{j \in \{0..m'\}}{\exists} art_{ycj} | art_{ycj} = art_{xdi} )) )$.
\end{definition}

\begin{definition}
  The set of activities $ACT$ is partitioned in stages denoted
  $Stg_1,Stg_2...Stg_n$.
\end{definition}

\begin{remark}
    Activities of different stages cannot be interleaved. Let $stg_1, stg2$ be two aritrary stages. $\forall act_1,act_2 \in ACT | ((act_1 \in stg_1 \land act_2 \in stg_2 \land act_1 < act_2) \implies (\nexists act_3 \in stg_1 | act_2 < act_3))$
\end{remark}


\begin{definition}
  The set of activities $ACT$ is partitioned in automated activities (denoted $Aut$) and manual activities (denoted $Man$).
\end{definition}




\subsection{Pipeline design}

The design of the pipeline is an implementation of the pipeline framework. The minimum set of stage is \textit{commit stage}, \textit{testing stage} and \textit{release stage}. The goal of the \textit{commit stage} is to produce a candidate. The \textit{testing stage} aims to detect defects in the candidate with user acceptance testing. The \textit{release stage} will release the candidate to production. The activity order is defined such that it respects the fail-fast principle \cite{HumbleFarley10} (p. 172). For instance, unit tests that execute faster than user acceptance tests will come before in the ordering.


\subsection{Pipeline environment design}

The design of the pipeline environment is given through a UML deployment diagram. The components necessary to support the activities of the pipeline are represented with a component diagram. Each component is then associated with an execution environment. An execution environment is a particular node on which components are executed. Please note that the physical piece of equipment on which the execution environments are deployed will not be detailed in this paper as the procedure we describe is not hardware-specific. The set of execution environment should at least contain the integration environment, the staging environment, and the production environment.\\
The integration execution environment will be composed of the components required to execute the continuous integration activities.
The production and staging execution environment will contain the module necessary to run the targeted product. The staging execution environment might be enhanced with the necessary modules needed for testing (e.g. user acceptance tests).

As stated in the book \cite{HumbleFarley10}(p. 258), a good practice is to keep the staging execution environment as close as possible as the production execution environment. This practice reduces the chance of system failure due to the difference in environments.

\subsection{Implementation of environment components}

Each component defined in the pipeline environment has to be mapped to an implementation. A single tool may embed multiple components.
The selection of the actual tools depends on the knowledge base of the team, the requirements, and the resources available.
The knowledge of the team has to be evaluated. It can take time and be error-prone to choose a complete unknown tool suite.
The tool chosen must be able to satisfy the stated requirements. Finally, it can be an advantage to adopt open-source tools to reduce costs.

\subsection{Pipeline scripting}

After choosing the set of tools of the environment we have to define the exact script of each automatic activities that will generate the output artefacts from the input ones and eventually alter the state (e.g. deployment). The scripts can rely on additional tools.

A recommended strategy is to develop, run, and test the script on a local environment before pushing it on the continuous integration server.

Once the script of each activity are validated (i.e. the script produce the defined output artefacts from the input artefacts), the pipeline script can be started.\\
The pipeline scripting is usually done through the chosen continuous integration tool. The pipeline script execution must follow the activity order and output defined previously. This can be done with a pipeline execution monitoring tool that is usually part of the continuous integration software.\\
The scripting process of the pipeline should be done iteratively in the order defined by the model. (e.g. commit stage, commit stage and testing, commit stage and testing and deployment)

\subsection{Experiment and measure the different requirements}

Finally, the non-functional requirements are verified with an empirical study. The conditions of the experiment need to be described and the exact method of measurement. The results of the experiment are compared to the satisfying condition of the requirement.
For instance, we can measure performance efficiency by comparing the time delta between commit and feedback considering the case with a pipeline and the case without a pipeline.
