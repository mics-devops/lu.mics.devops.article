# Content to be use in the article


## Subject elicitation and categorisation

### Cat 1 (Why)

- Architecture and environment schema + Justification (Showing how the components are related to the requirements)
- Automated pipeline schema + Actual code ref
- Justification on the different stage of our pipeline

### Cat 2 (How)

- Technologies used + Justification
- Describe the policy for VCS branching / overall operation (if no branching). It is necessary to ensure one (or more) of the functional requirement is fulfilled as a VCS misused might not fulfil this requirement.


### Cat 3 (why II)

- Non functional requirements imposed (Open source + OS unix based)
- Chosen non functional requirements + Justification (say we use standard)
- Present the metric and condition of measurements 3NFR chosen

### Cat 4 (results)

- Present the study case (App product) + Justification
- Functional requirements of the App product
- Result of the different experiment conduct.

### Other
- Maybe give a the ref of the book and say it is THE reference (actually check if it is THE reference)

## Subject mapping to IMRaD structure

### Research question
- Is it possible to improve the time performance, the maintainability and the reliability of the deployment process of am (existing/generic) product using DevOps principles and practices ?


### Introduction
- Non functional requirements imposed (Open source + OS unix based)
- Chosen NF requirements + Justification (say we use standard) shall it go to methods.
- Imposed functional requirements

### Methods
- Application of DevOps principles to design environment (staging is close to production/ integration is separated blablabla...) considering Non functional requirements
  - Diagram environment
- Automation of the creation of environment (this is also a principle)
- Justification of automatic pipeline usage (because of time)
- Define pipeline stages following principles (commit stage/test stage)
  - Diagram of generic pipeline
  - Describe the policy for VCS branching. It is necessary to ensure one (or more) of the functional requirement is fulfilled as a VCS misused might not fulfil this requirement.
- Case study :
  - Analyse current deployment process of the product on a local machine
  - As state in the book execution every task on command line
  - Creation of each state script
  - Measure of requirements

- Presentation of the product


### Result



- Technologies used + Justification for each component of the environment
- Usage of ansible because... summary of playbook
- Case study :
  - Current technologies use before pipeline
  - Proposition of the actual pipeline (techno to build)
  - Stage script
- Result of measure


### Discussion

### Conclusion


## Non-functional requirement proposition and measurements

- Performance efficiency (performance relative to the amount of resources used under stated conditions)
  - time behaviour (degree to which the response and processing times and throughput rates of a product or system, when performing its functions, meet requirements)

Measure: Time between commit and feedback (considering failure or success?) vs time without pipeline.  
Objective: Show improvement in the DevOps lifecycle in terms of time.  

- Maintainability (degree of effectiveness and efficiency with which a product or system can be modified by the intended maintainers)
  - Reusability (degree to which an asset can be used in more than one system, or in building other assets)

Measure: Time spent to adapt to the product chosen by another student.  
Threshold: to be determine.  
Objective: Show that our DevOps ecosystem can be adapted to different product having similar requirements.  
Further work: Use a python program as experiment otherwise state the limitation.  

- Reliability (degree to which a system, product or component performs specified functions under specified conditions for a specified period of time)
  - recoverability (degree to which, in the event of an interruption or a failure, a product or system can recover the data directly affected and re-establish the desired state of the system)

Measure1: time spent to redeploy the pipeline from a wipe / Revert changes that were already pushed to production.  
Measure2: ratio manual step/automated step (in terms of number or time to be determine) in the process.  
Objective: show that our product can be recovered in a limited amount of time and effort after a full crash / recover previous version of a part of the code.


## Figures

The following figures are expected for the report
- UML deployment diagram representing the environment
- UML architecture diagram showing how the components supporting the pipeline interact with each other
- UML architecture diagram showing what component aim at satisfying what requirement
- BPMN diagram showing the process of the automated pipeline
- Table summarising result of experiment 1
- Table summarising result of experiment 2
- Table summarising result of experiment 3
